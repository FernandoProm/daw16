<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html >
<head>
    <meta charset="utf-8"/>

    <title>Lab #9</title>
   
   
    <link rel="stylesheet" href="../CSS/Lab9.css">
</head>

<body>
    <div class="row">
    <div class="large-12 columns">
 
    </div>
        <center>
            
        </center>
        <br>
    </div>
    </div>
    <div class="row">
    <div class="large-12 columns">
        
<nav class="top-bar" data-topbar role="navigation">

  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="left">
        <li>
            <a class="active" href="Lab9.php"> Instituto Tecnológico y de Estudios Superiores de Monterrey</a>
        </li>
    </ul>
  </section>
</nav>
<br>
        

    
    <?php
        // define variables and set to empty values
        $firstName = $lastName = $studentID = $address = $postalCode = $email = $phone = $career = $average = $age = $gender = $comment  = "";
        $firstNameErr = $lastNameErr = $studentIDErr = $careerErr = $phoneErr = $ageErr = $genderErr = "";
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["firstName"])) {
                $firstNameErr = "El nombre es requerido";
            } else {
                $firstName = test_input($_POST["firstName"]);
                // check if first name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/",$firstName)) {
                    $firstNameErr = "Solo se aceptan Letras";
                    $firstName = "";
                }
            }
            
            if (empty($_POST["lastName"])) {
                $lastNameErr = "Apellido es requerido";
            } else {
                $lastName = test_input($_POST["lastName"]);
                // check if last name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/",$lastName)) {
                    $lastNameErr = "Solo se aceptan letras";
                    $lastName = "";
                }
            }
            
            if (empty($_POST["studentID"])) {
                $studentIDErr = "ID Requerido";
            } else {
                $studentID = test_input($_POST["studentID"]);
                if ($studentID[0] != 'A' || strlen($studentID) != 9 ) {
                    $studentIDErr = "Formato Invalido";
                    $studentID = "";
                }
            }
            
            if (empty($_POST["address"])) {
                $address = "";
            } else {
                $address = test_input($_POST["address"]);
            }
            
            if (empty($_POST["postalCode"])) {
                $postalCode = "";
            } else {
                $postalCode = test_input($_POST["postalCode"]);
            }
            
            if (empty($_POST["email"])) {
                $emailErr = "E-mail es requerido";
            } else {
                $email = test_input($_POST["email"]);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Formato invalido"; 
                    $email = "";
                }
            }   
            
            if (empty($_POST["phone"])) {
                $phoneErr = "Numero telefonico es obligatorio";
            } else {
                $phone = test_input($_POST["phone"]);
            }
            
            
            if (empty($_POST["career"])) {
                $careerErr = "Carrera obligatorio";
            } else {
                $career = test_input($_POST["career"]);
                if (strlen($career) > 3) {
                    $careerErr = "Formato Invalido";
                    $career = "";
                }
            }
            
            if (empty($_POST["average"])) {
                $averageErr = "Promedio obligatorio";
            } else {
                $average = test_input($_POST["average"]);
                if (((int)$average < 80) || ((int)$average > 100)) {
                    $averageErr = "Promedio solo entre 80 y 100";
                    $average = "";
                }
            }
            
            if (empty($_POST["age"])) {
                $ageErr = "Edad es obligatorio";
            } else {
                $age = test_input($_POST["age"]);
                // check if last name only contains numberst between 5 and 30
                if (((int)$age < 5) || ((int)$age > 25)) {
                    $ageErr = "Edad solo entre 5 y 25";
                    $age = "";
                }
            }
    
            if (empty($_POST["comment"])) {
                $comment = "";
            } else {
                $comment = test_input($_POST["comment"]);
            }
    
            if (empty($_POST["gender"])) {
                $genderErr = "Sexo es obligatorio";
            } else {
                $gender = test_input($_POST["gender"]);
            }
        }
        
        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
    ?>
    
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <h2>Formato de Solicitud de Becas Alimentarias</h2>
    <p><span class="err">(*) Required field.</span></p>
    <div class="large-12 columns">
    <div class="row">
    <div class="large-6 columns">
        Nombre*: <span class="err"> <?php echo $firstNameErr;?></span>
        <input type="text" name="firstName" placeholder="Ej. Fernando">
        
        Matricula*: <span class="err"> <?php echo $studentIDErr;?></span>
        <input type="text" name="studentID" placeholder="Ej. A00811303">
        
        Direccion: <input type="text" name="address">
        Codigo Postal: <input type="text" name="postalCode">
        
        Numero Telefonico*: <span class="err"> <?php echo $phoneErr;?></span>
        <input type="text" name="phone" placeholder="Ej. 442 305 9279">
        
     
    </div>
    <div class="large-6 columns">
        Apellido*: <span class="err"> <?php echo $lastNameErr;?></span>
        <input type="text" name="lastName" placeholder="Ej. Prom">
        
        Carrera*: <span class="err"> <?php echo $careerErr;?></span>
        <input type="text" name="career" placeholder="Ej. ISD, ITE, ISC">
        
        Promedio*: <span class="err"> <?php echo $averageErr;?></span>
        <input type="text" name="average">
        
        Email*: <span class="err"> <?php echo $emailErr;?></span>
        <input type="text" name="email" placeholder="Ej. fernandoprom31@gmail.com">
        
        Edad*: <span class="err"> <?php echo $ageErr;?></span>
        <input type="text" name="age">
        
        Sexo*: <span class="err"> <?php echo $genderErr;?></span> <br>
        <input type="radio" name="gender" value="Mujer"> Mujer &nbsp;&nbsp;
        <input type="radio" name="gender" value="Hombre"> Hombre
        <br>
    </div>
    </div>
    </div>
    
    <div class="large-12 columns">
    <div class="row">
    <div class="large-7 columns">
        Comentarios: <textarea name="comment" rows="5" cols="20"></textarea>
        <input type="submit" name="submit" value="Submit"> <br><br>
    </div>
    </div>
    </div>
    </form>

    <?php
        if ($_POST["submit"] == "Submit") {
            echo "<hr>";
            echo "<h2>INFORMACION PERSONAL FINAL</h2>";
            echo "Nombre: "; echo $firstName; echo " "; echo $lastName; echo "<br>";
            echo "Edad: "; echo $age; echo "<br>";
            echo "Sexo: "; echo $gender; echo "<br>";
            echo "Matricula: "; echo $studentID; echo "<br>";
            echo "Carrera: "; echo $career; echo "<br>";
            echo "Direccion: "; echo $address; echo "<br>";
            echo "Promedio: "; echo $average; echo "<br>";
            echo "Codigo Postal: "; echo $postalCode; echo "<br>";
            echo "Numero Telefonico: "; echo $phone; echo "<br>";
            echo "Email: "; echo $email; echo "<br>";
            echo "Comentarios: "; echo $comment; echo "<br>";
        }
    ?>
    
    <hr>
    <h2>Preguntas</h2>
    <h4>1. ¿Por qué es una buena práctica separar el controlador de la vista?</h4>
    
        Dividir nuestro proyecto en capas nos facilita el manejo del mismo, así como poder sustituir capas de manera más rápidad y eficiente. Se logra hacer una rápida distinción entre lo que se muestra contra lo que controla y el output final.
    <h4><br>2. Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?</h4>
		
        <strong>- $_POST:</strong> matriz que maneja variables obtenidas por el método HTTP 	 
    <br><strong>- $_GET:</strong> matriz que maneja variables obtenidas mediante parámetros URL. 
    <br><strong>- $_COOKIE:</strong> matriz que maneja variables obtenidas mediante HTTP Cookies.
    <br><strong>- $_REQUEST:</strong> matriz que maneja variables variables relacionadas con todo lo que contiene $_GET, $_POST y $_COOKIE.

    <h4><br>3. Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.</h4>

       <br><strong>function_exists(function_name):</strong> Devuelve True si la función ya fue definida.
       <br><strong>var_export:</strong> Devuelve una representación string de una variable.

    <br> <br> <br>
    
    
   
  
</body>
</html>