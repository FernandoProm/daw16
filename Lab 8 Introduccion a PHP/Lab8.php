<!DOCTYPE html>
<html>
   
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="content-type">
        <link rel="stylesheet" type= "text/css" href="../CSS/Lab8.css" />
        <h1>
            Fernando Prom de la Rosa<br>
            
            Laboratorio #8<br>
        </h1>
    </head>
    
    <body>
        <?php //Crea arreglos y despliegalos
            $array1 = array(10,2,1,9,4,5,7,10,7,3,7,4,2,5,2,5,6,8);
            $array2 = array(2,8,9,4,2,3,4,5,0,6,7,5,4,2,2,3,9,8,7,0,10,1,10,5,4,3,0);
        
            //Llama funciones
            lista($array1, 1);
            lista($array2, 2);
            
            echo '<form method="POST" action="Lab8.php">
                        Numero entero positivo: 
                        <input type="text" name="num">
                        <input type="submit" name ="submit" value="Enviar">
                 </form><br>';
                 
            if ($_POST["submit"] == "Enviar") {
                  cubos($_POST["num"]);
            } 
            
            echo '<form method="POST" action="Lab8.php">
                        Numero entero positivo a invertir: 
                        <input type="text" name="num2">
                        <input type="submit" name ="submit2" value="Enviar">
                 </form><br>';
                 
            if ($_POST["submit2"] == "Enviar") {
                  invierte($_POST["num2"]);
            } 
            
        ?>
        
        <?php //Funcion que imprime un arreglo
            function display($array) {
                echo "[";
                for ($i = 0; $i < sizeof($array); $i++) {
                    if ($i < sizeof($array) - 1) {
                        echo "$array[$i], ";
                    } else {
                        echo "$array[$i]]<br>";
                    }
                }
            }
        ?>
        
        <?php //Función que reciba un arreglo de números y devuelva su promedio
            function promediador($array) {
                $x = 0;
                
                for ($i = 0; $i < sizeof($array); $i++) {
                    $x += $array[$i];
                }
                $x = $x/sizeof($array);
                return $x;
            }
        ?>
        
        <?php //Función que reciba un arreglo de números y devuelva su mediana
            function mediana($array) {
                $middle;
                $median;
                sort($array);

                $middle = sizeof($array)/2;
                $median = $array[$middle];
                if (sizeof($array) % 2 == 0) {
                    $median = ($median + $array[$middle - 1]) / 2;
                }
                return $median;
            }
        ?>
        
        <?php //Función que reciba un arreglo de números y muestre la lista de números, 
              //y como ítems de una lista html muestre el promedio, la media, y el arreglo 
              //ordenado de menor a mayor, y posteriormente de mayor a menor.
            function lista($array, $num) {
                $lowToHigh = $highToLow = $array;
                sort($lowToHigh);
                rsort($highToLow);
                
                echo "<br><ul>
                            <li>Arreglo $num:
                                <ul>
                                    <li>";
                                        display($array);
                                    echo "</li>
                                </ul>
                            </li>
                            <li>Promedio:
                                <ul>
                                    <li>" . promediador($array) . "</li>
                                </ul>
                            </li>
                            <li>Media:
                                <ul>
                                    <li>" . mediana($array) . "</li>
                                </ul>
                            </li>
                            <li>Ordenado menor a mayor:
                                <ul>
                                    <li>";
                                        display($lowToHigh);
                              echo "</li>
                                </ul>
                            </li>
                            <li>Ordenado mayor a menor:
                                <ul>
                                    <li>";
                                        display($highToLow);
                              echo "</li>
                                </ul>
                            </li>
                        </ul><br><br>";
            }
        ?>
        
        <?php //Función que imprima una tabla html, que muestre los cuadrados y cubos de 1 hasta un número n
            function cubos($num) {
                $temp;

                echo "<table><tr><th>Enteros</th><th>Cuadrados</th><th>Cubos</th></tr>";

                for ($i = 0; $i < $num; $i++) {
                    $temp=$i+1;
                    echo "<tr><td>" . pow($temp,1) . "</td>";
                    echo "<td>"     . pow($temp,2) . "</td>";
                    echo "<td>"     . pow($temp,3) . "</td></tr>";
                }
                echo "</table><br>";
            }
        ?>
        
        <?php //Función que imprima una tabla html, que muestre los cuadrados y cubos de 1 hasta un número n
            function invierte($num) {
                
                echo "Normal: " . $num . "<br>";
                echo "Invertido: ";
                while($num != 0){
                    echo $num % 10;
                    $num = ( $num - $num % 10 ) / 10;
                }
                
                echo "<br><br>";
            }
        ?>
        
        <h1>Preguntas</h1>
        <h>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</h>
        <p>Al llamar esta funcion, la interfaz muestra toda la información sobre la configuración de PHP.<br>

            Como por ejemplo: <br>
            INFO_GENERAL: la linea de configuración.<br>
            INFO_CREDITS: creditos de PHP. <br>
            INFO_CONFIGURATION: valores Local y Master para directivas PHP.<br><br>
            
            
        <h>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</h>
        <p>Procesar un archivo PHP en tu servidor es necesario instalarle el software (por ejemplo Apache) para que pueda entender y traducir todo el documento e interacciones con el usuario.
</p>
        <h>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</h>
        <p>El servidor procesa el archivo PHP y la respuesta la traduce a un archivo HTML que es el que se envia al cliente para que lo observe.</p>
        
    </body>
    
    <footer>
        
        
        
    </footer>
    
</html>