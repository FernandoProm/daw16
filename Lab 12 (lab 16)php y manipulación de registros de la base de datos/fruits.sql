-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-03-2016 a las 18:28:09
-- Versión del servidor: 5.5.44-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `DataBase`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fruits`
--

CREATE TABLE IF NOT EXISTS `fruits` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Unidades` int(11) NOT NULL,
  `Cantidad` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Precio` int(11) NOT NULL,
  `Pais` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fruits`
--

INSERT INTO `fruits` (`id`, `Nombre`, `Unidades`, `Cantidad`, `Precio`, `Pais`) VALUES
(0, 'Manzana', 2, 'Kg', 50, 'Mexico'),
(1, 'Mango', 50, 'Kg', 79, 'Alemania'),
(2, 'Aguacate', 70, 'Kg', 15, 'Francia'),
(2, 'Aguacate', 70, 'Kg', 15, 'Francia');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
