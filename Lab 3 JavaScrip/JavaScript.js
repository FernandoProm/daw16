document.write("<h2>Ejercicio 1</h2>");

var size = window.prompt("EJERCICIO 1:    Dame un numero entero: ");
var i, y;

document.write("<strong>  TABLA</strong><br><table><tr><th>Enteros</th><th>Cuadrados</th><th>Cubos</th></tr></strong>");

for (i = 0; i < size; i++) {
        y=i+1;
        document.write("<tr><td>" + y + "</td>");
        document.write("<td>" + Math.pow(y,2) + "</td>");
        document.write("<td>" + Math.pow(y,3) + "</td></tr>");
}

document.write("</table><br>");


///////////
/////////
////////////
///////////////

    num1 = Math.floor(Math.random()*100);
    num2 = Math.floor(Math.random()*100);
   
var startTime = Date.now(); //Start timer
var resp = window.prompt("Ejercicio 2 Dame el resultado de " + num1 + " + " + num2 +" ");
var stopTime = Date.now(); //Stop Timer


document.write("<h2>Ejercicio 2</h2>");

document.write("<strong>Suma</strong><br>   " + num1 + " + " + num2 + " = " + resp);
if (resp== (num1+num2)) {
        
        document.write("<br>Respuesta CORRECTA");     
} else {
        
        document.write("<br>Respuesta INCORRECTA"); 
}

document.write("<br>Tiempo: " + ((stopTime - startTime)/1000) + " seg<br>");




//////////////////////////////////
///////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////////////

document.write("<h2>Ejercicio 3</h2>");

var arr = new Array(30);
var ceros=0, negativos=0, positivos=0;

for (i=0;i < arr.length;i++){
        arr[i] = Math.floor((Math.random() * 10) - 5);        
}

for (i=0;i < arr.length;i++){
        if (arr[i]==0) {
                ceros++;          
        } else if (arr[i]>0){
                positivos++;
        } else {
                negativos++;
        }
}

document.write("<br><strong>Contador</strong><br>");
document.write("Arreglo = [");
for (i=0;i < arr.length-1;i++){
        document.write(arr[i] + ", ");       
}
document.write(arr[arr.length-1] + "]<br>");
document.write("<br>Ceros: " + ceros + "");
document.write("<br>Negativos: " + negativos + "");
document.write("<br>Positivos: " + positivos + "");




/////////////////////////////////////7
///////////////////////////////77
///////////////////////////7

document.write("<h2>Ejercicio 4</h2>");

var matrix = [];
var j;
var prom=0;
document.write("<br>");
document.write("<strong>Promedio</strong><br>");
for(i = 0; i < 10; i++){
    matrix[i] = [];
    document.write("Renglon " + i +" --| ");
    for(j = 0; j < 9; j++){ 
        matrix[i][j] = Math.floor((Math.random() * 10));
        prom = matrix[i][j] + prom;
        document.write(matrix[i][j] + " | ");
    }   
   matrix[i][j] = Math.floor((Math.random() * 10));
   prom = matrix[i][j] + prom;
   document.write(matrix[i][j] + "|  Promedio = " + (prom/10) + " <br>");
    prom=0;
}


/////////////////////////////////77
/////////////////////////////////////
/////////////////////////////7

document.write("<h2>Ejercicio 5</h2>");

var inverso = window.prompt("Dame el numero a invertir: ");
document.write("<strong>Inverso</strong><br>");

document.write("Normal: " + inverso + "<br>");
document.write("Inverso:");
while(inverso!=0){
       document.write(inverso%10);
       inverso = (inverso - inverso%10)/10;
}
////////////////////////////////////77
////////////////////////////////////////

document.write("<h2>PREGUNTAS</h2>");


document.write("<strong>Que diferencias y semejanzas hay entre Java y JavaScript?</strong>");

document.write("<br>Javascript se ejecuta en el navegador, mientras que Java no lo hace. Ambos lenguajes fueron creados dirigidos a Objetos Orientados");

document.write("<br><br><strong>Que metodos tiene el objeto Date?</strong>");

document.write("<br> Date.prototype.getDay(): Regresa el dia de la semana ");
document.write("<br> Date.prototype.getSeconds(): Regresa los segundo de la hora local");
document.write("<br> Date.prototype.getMonth() Regresa el mes de acuerdo a la fecha local");
document.write("<br>Date.prototype.getMinutes(): Regresa los minutos de la hora local");


document.write("<br><br><strong>Que metodos tienen los arreglos?</strong> ");

document.write("<br>Push: agrega nuevos elementos al arreglo");
document.write("<br>Pop: remueve el ultimo elemento del arrglo");
document.write("<br>Join: ingresa todos los elementos del arreglo a un string");
document.write("<br>Valueof: regresa el arreglo como un string");
document.write("<br>Shift: remueve el primer elemento del arreglo y cambia la posicion de todos uno hacia abajo");

document.write("<br><br><strong>Como se declara una variable con alcance local dentro de una funcion? </strong>");

document.write("<br> var = Nombre_Variable; dentro de una funcion");

document.write("<br><br><strong>Que implicaciones tiene utilizar variables globales dentro de funciones? </strong>");

document.write("<br> Se podría perder el control de las variables, por ejemplo volver a repetir nombre de Variables si es que se repiten en varias funciones ");

document.write("<br><br><strong>Que metodo de String se puede utilizar para buscar patrones con expresiones regulares? Para que podrias utilizar esto en una aplicación web? </strong>");

document.write("<br> exec: Busca el string deseado y regresa si encontro algo o no, y se puede utilizar en strings muy largos");